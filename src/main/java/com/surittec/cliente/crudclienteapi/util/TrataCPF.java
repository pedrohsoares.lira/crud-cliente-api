package com.surittec.cliente.crudclienteapi.util;

public class TrataCPF {
	public static String retiraMascaraCpf(String cpf) {
	      if (cpf==null) {
	        return "";
	      }
	      return cpf.replaceAll("\\.|-|/", "");
	    }
	    
	    public static String adicionaMascaraCpf(String cpf) {
	      return cpf.replaceAll("(\\d{3})(\\d{3})(\\d{3})", "$1.$2.$3-");
	    }
}
