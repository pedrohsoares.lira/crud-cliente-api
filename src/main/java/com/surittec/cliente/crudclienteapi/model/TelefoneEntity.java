package com.surittec.cliente.crudclienteapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.surittec.cliente.crudclienteapi.model.enuns.TipoTelefoneEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "telefone")
public class TelefoneEntity {

	 	@Id
	    @Column(name = "id_telefone")
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	    @Column
	    private String numero;

	    @Enumerated(EnumType.ORDINAL)
	    @Column(name = "tipo_telefone")
	    private TipoTelefoneEnum tipoTelefone;

	    @ManyToOne
	    @JoinColumn(name = "cliente", referencedColumnName = "id_cliente", nullable = false)
	    private ClienteEntity cliente;

	
}
