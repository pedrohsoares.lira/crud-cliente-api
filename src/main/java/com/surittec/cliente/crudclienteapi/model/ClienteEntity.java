package com.surittec.cliente.crudclienteapi.model;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.surittec.cliente.crudclienteapi.model.dto.ClienteDTO;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "cliente")
@NoArgsConstructor
public class ClienteEntity {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cliente")
	private Long id;
	
	@Column
	private String nome;
	@Column
	private String cpf;
	
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<EmailEntity> emails;
	
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<TelefoneEntity> telefones;

	@OneToOne(mappedBy = "cliente", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_cliente", referencedColumnName = "cliente")
    private EnderecoEntity endereco;
	
	@Column(name = "dt_criado", updatable = false)
    @CreatedDate
    private LocalDateTime dtCriado;

    @Column(name = "dt_modificado")
    @LastModifiedDate
    private LocalDateTime dtModificado;

    @CreatedBy
    @Column(name = "no_responsavel_cadastro", updatable = false)
    private String noResponsavelCadastro;

    @LastModifiedBy
    @Column(name = "no_responsavel_alteracao")
    private String noResponsavelAlteracao;
    
    public ClienteEntity(ClienteDTO dto) {
    	this.id = dto.getId();
    	this.nome = dto.getNome();
    	this.cpf = dto.getCpf();
    	this.emails = dto.getEmails();
    	this.telefones = dto.getTelefones();
    	this.endereco = dto.getEndereco();
    }

	public void atualizarCadastro(ClienteDTO clienteDTO) {
		this.nome = clienteDTO.getNome();
		this.emails = clienteDTO.getEmails();
		this.telefones = clienteDTO.getTelefones();
		this.endereco = clienteDTO.getEndereco();
	}
    
}