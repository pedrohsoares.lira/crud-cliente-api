package com.surittec.cliente.crudclienteapi.model.enuns;

public enum Perfil {

	ADMINISTRADOR(0, "Administrador"), COMUM(1, "Comum");

	private Perfil(Integer cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}
	
	private Integer cod;
	private String descricao;
	
	public Integer getCod() {
		return cod;
	}
	public String getDescricao() {
		return descricao;
	}

	public static Perfil toEnum(Integer cod) {
		if (cod == null) {
			return null;
		}
		
		for (Perfil p : Perfil.values()) {
			if (cod.equals(p.getCod())) {
				return p;
			}
		}
		
		throw new IllegalArgumentException("Tipo de Perfil inválido " + cod);
	}
}
