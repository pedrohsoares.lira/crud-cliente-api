package com.surittec.cliente.crudclienteapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "email")
@Getter
@Setter
@EqualsAndHashCode
public class EmailEntity {
	
		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "id_cliente")
	    private Long id;

	    @Column
	    private String email;

	    @ManyToOne
	    @JoinColumn(name = "cliente", referencedColumnName = "id_cliente", nullable = false)
	    private ClienteEntity cliente;

}