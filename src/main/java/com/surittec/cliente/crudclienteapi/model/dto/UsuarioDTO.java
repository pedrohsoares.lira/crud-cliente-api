package com.surittec.cliente.crudclienteapi.model.dto;

import javax.validation.constraints.NotNull;

import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDTO {

    private Long id;
    private String nome;
    @NotNull
    private String login;
    @NotNull
    private String password;
    private int perfil;
    private Boolean  stAtivo;
	
    
    public static UsuarioDTO toDTO(UsuarioEntity entity) {
    	return UsuarioDTO.builder().id(entity.getId()).login(entity.getLogin()).nome(entity.getNome()).password(entity.getPassword()).perfil(entity.getPerfil().getCod()).stAtivo(entity.getStAtivo()).build();
    }
}
