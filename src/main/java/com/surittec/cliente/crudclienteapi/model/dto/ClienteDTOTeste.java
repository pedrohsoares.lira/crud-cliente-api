package com.surittec.cliente.crudclienteapi.model.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.surittec.cliente.crudclienteapi.model.ClienteEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ClienteDTOTeste {

	private Long id;
	private String nome;
	private String cpf;
	private String email;

    
    public ClienteDTOTeste (ClienteEntity c) {
    	this.id = c.getId();
    	this.nome = c.getNome();
    	this.cpf = c.getCpf();
    }
    
    public static List<ClienteDTOTeste> converter(List<ClienteEntity> lista) {
    	return lista.stream().map(ClienteDTOTeste::new).collect(Collectors.toList());
    }
}
