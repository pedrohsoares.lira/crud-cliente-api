package com.surittec.cliente.crudclienteapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.surittec.cliente.crudclienteapi.model.enuns.Perfil;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@Table(name = "usuario")
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario", nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false, length = 100)
    private String nome;

    @Column(nullable = false, length = 45)
    private String login;

    @Column(nullable = false, length = 255)
    private String password;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private Perfil perfil;

    @Column(nullable = false)
    private Boolean  stAtivo;
		
}
