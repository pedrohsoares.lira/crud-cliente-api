package com.surittec.cliente.crudclienteapi.model.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import com.surittec.cliente.crudclienteapi.model.ClienteEntity;
import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;
import com.surittec.cliente.crudclienteapi.model.dto.ClienteDTO;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ClienteMapper extends BaseMapper<ClienteEntity, ClienteDTO> {

    @InheritInverseConfiguration(name = "toDto")
    void fromDto(ClienteDTO dto, @MappingTarget UsuarioEntity entity);

    @Named("partialUpdate")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void partialUpdate(@MappingTarget ClienteEntity entity, ClienteDTO dto);
}
