package com.surittec.cliente.crudclienteapi.model.enuns;

public enum TipoTelefoneEnum {

	COMERCIAL(0, "Comercial"), CELULAR(1, "Celular"), RESIDENCIAL(2, "Residencial");
	
	private TipoTelefoneEnum(Integer cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}
	
	private Integer cod;
	private String descricao;
	
	public Integer getCod() {
		return cod;
	}
	public String getDescricao() {
		return descricao;
	}

	public static TipoTelefoneEnum toEnum(Integer cod) {
		if (cod == null) {
			return null;
		}
		
		for (TipoTelefoneEnum p : TipoTelefoneEnum.values()) {
			if (cod.equals(p.getCod())) {
				return p;
			}
		}
		
		throw new IllegalArgumentException("Tipo de Telefone inválido " + cod);
	}
	
}
