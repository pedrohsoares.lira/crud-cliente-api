package com.surittec.cliente.crudclienteapi.model.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;
import com.surittec.cliente.crudclienteapi.model.dto.UsuarioDTO;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UsuarioMapper extends BaseMapper<UsuarioEntity, UsuarioDTO> {

    @InheritInverseConfiguration(name = "toDto")
    void fromDto(UsuarioDTO dto, @MappingTarget UsuarioEntity entity);

}
