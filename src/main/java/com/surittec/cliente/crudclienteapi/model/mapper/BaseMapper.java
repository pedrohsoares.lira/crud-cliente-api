package com.surittec.cliente.crudclienteapi.model.mapper;

import java.util.List;

import org.mapstruct.MappingTarget;

public interface BaseMapper<E, D> {

    D toDto(E entity);

    E toEntity(D dto);

    List<D> toDto(List<E> entities);

    List<D> toDto(Iterable<E> entities);

    List<E> toEntity(List<D> dtos);

    void fromDto(D dto, @MappingTarget E entity);

}