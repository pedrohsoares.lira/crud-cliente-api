package com.surittec.cliente.crudclienteapi.model.dto;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;

import com.surittec.cliente.crudclienteapi.model.ClienteEntity;
import com.surittec.cliente.crudclienteapi.model.EmailEntity;
import com.surittec.cliente.crudclienteapi.model.EnderecoEntity;
import com.surittec.cliente.crudclienteapi.model.TelefoneEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ClienteDTO {

	private Long id;
	@NotNull
    @Size(min = 3, max = 100)
	private String nome;
	@NotNull
    @CPF
	private String cpf;
	@Valid
    @NotEmpty
    private Set<EmailEntity> emails;
	@Valid
    @NotEmpty
    private Set<TelefoneEntity> telefones;
	@Valid
    @NotNull
    private EnderecoEntity endereco;

    
    public ClienteDTO (ClienteEntity c) {
    	this.id = c.getId();
    	this.nome = c.getNome();
    	this.cpf = c.getCpf();
    	this.emails = c.getEmails();
    	this.telefones = c.getTelefones();
    	this.endereco = c.getEndereco();
    }
    
    public static List<ClienteDTO> converter(List<ClienteEntity> lista) {
    	return lista.stream().map(ClienteDTO::new).collect(Collectors.toList());
    }
}
