package com.surittec.cliente.crudclienteapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;

public interface UsuarioRespository extends JpaRepository<UsuarioEntity, Long>  {
	
	boolean existsByLogin(String login);

	Optional<UsuarioEntity> findByLogin(String login);
}
