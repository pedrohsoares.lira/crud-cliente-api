package com.surittec.cliente.crudclienteapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.surittec.cliente.crudclienteapi.model.ClienteEntity;

@Repository
public interface ClienteRepository extends JpaRepository<ClienteEntity, Long> {
	
	@Query("SELECT obj FROM ClienteEntity obj WHERE obj.cpf = :cpf")
	ClienteEntity getByCPF(@Param("cpf") String cpf);

}
