package com.surittec.cliente.crudclienteapi.service;

import java.util.List;
import java.util.Optional;

import com.surittec.cliente.crudclienteapi.model.ClienteEntity;
import com.surittec.cliente.crudclienteapi.model.dto.ClienteDTO;

public interface ClienteService {
	
	ClienteDTO cadastrar(ClienteDTO clienteDTO);
	ClienteDTO atualizar(Long id, ClienteDTO clienteDTO);
	List<ClienteDTO> buscarTodos();
	void excluir(Long id);
	ClienteEntity buscar(Long id);
	Optional<ClienteEntity> obterPorId(Long id);

}
