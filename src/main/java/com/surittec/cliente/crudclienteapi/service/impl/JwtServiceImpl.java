package com.surittec.cliente.crudclienteapi.service.impl;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;
import com.surittec.cliente.crudclienteapi.service.JwtService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtServiceImpl implements JwtService{
	
	@Value("${jwt.expiracao}")
	private String expiracao;
	
	@Value("${jwt.chave-assinatura}")
	private String chaveAssinatura;

	@Override
	public String gerarToken(UsuarioEntity usuario) {
		long exp = Long.valueOf(expiracao);
		LocalDateTime dataHoraExpiracao = LocalDateTime.now().plusMinutes(exp);
		Instant instant = dataHoraExpiracao.atZone(ZoneId.systemDefault()).toInstant();
		Date data = Date.from(instant);
		String token = Jwts
						.builder()
						.setExpiration(data)
						.setSubject(usuario.getLogin())
						.claim("userId", usuario.getId())
						.claim("nome", usuario.getNome())
						.claim("permissao", usuario.getPerfil().getDescricao())
						.claim("horaExpiracao", dataHoraExpiracao.toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm")))
						.signWith(SignatureAlgorithm.HS512, chaveAssinatura)
						.compact();
						
		return token;
	}

	@Override
	public Claims obterClaims(String token) throws ExpiredJwtException {
		return Jwts.parser()
					.setSigningKey(chaveAssinatura)
					.parseClaimsJws(token)
					.getBody();
	}

	@Override
	public boolean isTokenValido(String token) {
		try {
			Claims clains = obterClaims(token);
			Date expiration = clains.getExpiration();
			LocalDateTime dataExpiracao = expiration.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			return !LocalDateTime.now().isAfter(dataExpiracao);
		} catch (ExpiredJwtException e) {
			return false;
		}
	}

	@Override
	public String obterLoginUsuario(String token) {
		Claims clains = obterClaims(token);
		return clains.getSubject();
	}

}
