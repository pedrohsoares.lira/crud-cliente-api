package com.surittec.cliente.crudclienteapi.service;

import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;

public interface JwtService {
	
	String gerarToken(UsuarioEntity usuario);
	
	Claims obterClaims(String token) throws ExpiredJwtException;
	
	boolean isTokenValido(String token);
	
	String obterLoginUsuario(String token);

}
