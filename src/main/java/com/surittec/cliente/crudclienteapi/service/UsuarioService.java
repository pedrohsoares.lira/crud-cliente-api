package com.surittec.cliente.crudclienteapi.service;

import java.util.List;

import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;

public interface UsuarioService {

	UsuarioEntity autenticar(String login, String senha);
	
	UsuarioEntity salvarUsuario(UsuarioEntity usuarioEntity);
	
	void validarLogin(String login);

	List<UsuarioEntity> buscar();
	
}
