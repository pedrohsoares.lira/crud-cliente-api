package com.surittec.cliente.crudclienteapi.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.surittec.cliente.crudclienteapi.exception.ErroAutenticacao;
import com.surittec.cliente.crudclienteapi.exception.RegraNegocioException;
import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;
import com.surittec.cliente.crudclienteapi.repository.UsuarioRespository;
import com.surittec.cliente.crudclienteapi.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService{
	
	private UsuarioRespository repository;
	private PasswordEncoder encoder;

	public UsuarioServiceImpl(UsuarioRespository repository, PasswordEncoder encoder) {
		super();
		this.repository = repository;
		this.encoder = encoder;
	}
	
	@Override
	public UsuarioEntity autenticar(String login, String senha) {
		Optional<UsuarioEntity> usuario = repository.findByLogin(login);
		if (!usuario.isPresent()) {
			throw new ErroAutenticacao("Usuário não encontrado para o Login informado.");
		}
		if (!senhasBatem(senha, usuario)) {
			throw new ErroAutenticacao("Senha inválida.");
		}
		return usuario.get();
	}

	private boolean senhasBatem(String senha, Optional<UsuarioEntity> usuario) {
		return encoder.matches(senha, usuario.get().getPassword());
	}

	@Override
	@Transactional
	public UsuarioEntity salvarUsuario(UsuarioEntity usuarioEntity) {
		validarLogin(usuarioEntity.getLogin());
		criptografarSenha(usuarioEntity);
		return repository.save(usuarioEntity);
	}

	private void criptografarSenha(UsuarioEntity usuarioEntity) {
		String senha = usuarioEntity.getPassword();
		String senhaCriptografada = encoder.encode(senha);
		usuarioEntity.setPassword(senhaCriptografada);
	}

	@Override
	public void validarLogin(String login) {
		boolean existe = repository.existsByLogin(login);
		if (existe) {
			throw new RegraNegocioException("Já existe um usuario cadastrado com esse login");
		}
		
	}

	@Override
	public List<UsuarioEntity> buscar() {
		return repository.findAll();
	}

}
