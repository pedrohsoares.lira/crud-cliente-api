package com.surittec.cliente.crudclienteapi.service.impl;

import java.util.List;
import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.surittec.cliente.crudclienteapi.exception.DataIntegratyViolationException;
import com.surittec.cliente.crudclienteapi.model.ClienteEntity;
import com.surittec.cliente.crudclienteapi.model.dto.ClienteDTO;
import com.surittec.cliente.crudclienteapi.repository.ClienteRepository;
import com.surittec.cliente.crudclienteapi.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public ClienteDTO cadastrar(ClienteDTO clienteDTO) {
		if (clienteRepository.getByCPF(clienteDTO.getCpf())!= null) {
			throw new DataIntegratyViolationException("CPF já cadastrado na base de dados.");
		}
		ClienteEntity ent = clienteRepository.save(new ClienteEntity(clienteDTO)); 
		return new ClienteDTO(ent);
	}

	@Override
	public ClienteDTO atualizar(Long id, ClienteDTO clienteDTO) {
		ClienteEntity atualizar = this.buscar(id);
		atualizar.atualizarCadastro(clienteDTO);
		clienteRepository.save(atualizar);
		return new ClienteDTO(atualizar);
	}
	
	@Override
	public List<ClienteDTO> buscarTodos() {
		List<ClienteEntity> cEntity = clienteRepository.findAll();
		return ClienteDTO.converter(cEntity);
	}

	@Override
	public void excluir(Long id) {
		ClienteEntity excluir = this.buscar(id);
		clienteRepository.delete(excluir);
	}

	@Override
	public ClienteEntity buscar(Long id) {
		Optional<ClienteEntity> opt = clienteRepository.findById(id);
		return opt.orElseThrow(() -> new ObjectNotFoundException("Cliente não encontrado! Id: " + id + ", tipo: " + ClienteEntity.class.getName(), null));
	}

	@Override
	public Optional<ClienteEntity> obterPorId(Long id) {
		return clienteRepository.findById(id);
	}

}
