package com.surittec.cliente.crudclienteapi.service.impl;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;
import com.surittec.cliente.crudclienteapi.repository.UsuarioRespository;

@Service
public class SecurityUserDetailsService implements UserDetailsService{

	private UsuarioRespository uRepository;

	public SecurityUserDetailsService (UsuarioRespository uRepository) {
		this.uRepository = uRepository;
		
	}
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		UsuarioEntity usuario = uRepository.findByLogin(login).orElseThrow(() -> new UsernameNotFoundException("Login não cadastrado"));
		return User.builder()
				.username(usuario.getLogin())
				.password(usuario.getPassword())
				.roles(usuario.getPerfil().getDescricao())
				.build();
	}

}
