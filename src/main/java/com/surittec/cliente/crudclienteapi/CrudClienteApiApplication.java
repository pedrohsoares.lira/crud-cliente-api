package com.surittec.cliente.crudclienteapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class CrudClienteApiApplication implements WebMvcConfigurer {
	
	public static void main(String[] args) {
		SpringApplication.run(CrudClienteApiApplication.class, args);
	}

}
