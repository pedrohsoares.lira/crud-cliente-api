package com.surittec.cliente.crudclienteapi.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.surittec.cliente.crudclienteapi.exception.ErroAutenticacao;
import com.surittec.cliente.crudclienteapi.exception.RegraNegocioException;
import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;
import com.surittec.cliente.crudclienteapi.model.dto.TokenDTO;
import com.surittec.cliente.crudclienteapi.model.dto.UsuarioDTO;
import com.surittec.cliente.crudclienteapi.model.enuns.Perfil;
import com.surittec.cliente.crudclienteapi.service.JwtService;
import com.surittec.cliente.crudclienteapi.service.UsuarioService;

@CrossOrigin("*")
@RequestMapping("/api/usuarios")
@RestController
public class UsuarioController {

	private UsuarioService service;
	private JwtService jwtService;
	
	public UsuarioController(UsuarioService service, JwtService jwtService) {
		this.service = service;
		this.jwtService = jwtService;
	}
	
	@PostMapping("/autenticar")
	public ResponseEntity<?> autenticar(@RequestBody UsuarioDTO dto) {
		try {
			UsuarioEntity usuarioAutenticado = service.autenticar(dto.getLogin(), dto.getPassword());
			String token = jwtService.gerarToken(usuarioAutenticado);
			
			TokenDTO tokenDTO = new TokenDTO(usuarioAutenticado.getNome(), token);
			return ResponseEntity.ok(tokenDTO);
		} catch (ErroAutenticacao e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping
	public ResponseEntity salvar( @RequestBody UsuarioDTO dto) {
		UsuarioEntity uEntity = UsuarioEntity.builder().login(dto.getLogin()).nome(dto.getNome()).password(dto.getPassword()).perfil(Perfil.toEnum(dto.getPerfil())).stAtivo(true).build();
		
		try {
			UsuarioEntity usuarioSalvo = service.salvarUsuario(uEntity);
			return new ResponseEntity(UsuarioDTO.toDTO(usuarioSalvo), HttpStatus.CREATED);
			}catch (RegraNegocioException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@GetMapping
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResponseEntity buscar() {
		return new ResponseEntity(service.buscar(), HttpStatus.OK);
	}

}
