package com.surittec.cliente.crudclienteapi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.surittec.cliente.crudclienteapi.model.ClienteEntity;
import com.surittec.cliente.crudclienteapi.model.dto.ClienteDTO;
import com.surittec.cliente.crudclienteapi.service.ClienteService;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/clientes")
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;

	@PostMapping()
	public ResponseEntity<ClienteDTO> cadastrarCliente(@Valid @RequestBody ClienteDTO clienteDTO) {
		ClienteDTO cadastrado = clienteService.cadastrar(clienteDTO);
		return ResponseEntity.ok().body(cadastrado);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<ClienteDTO> atualizarCliente(@Valid @RequestBody ClienteDTO clienteDTO, @PathVariable Long id) {
		ClienteDTO atualizado = clienteService.atualizar(id, clienteDTO);
		return ResponseEntity.ok().body(atualizado);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> excluirCliente(@PathVariable Long id) {
		clienteService.excluir(id);
		return ResponseEntity.noContent().cacheControl(CacheControl.noCache()).build();
	}
	
	@GetMapping()
	public ResponseEntity<List<ClienteDTO>> listarTodosClientes() {
		List<ClienteDTO> clientes = clienteService.buscarTodos();
		return ResponseEntity.ok().body(clientes);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("{id}")
	public ResponseEntity obterCliente(@PathVariable("id") Long id) {
		return clienteService.obterPorId(id)
					.map( clienteEntity -> new ResponseEntity(converter(clienteEntity), HttpStatus.OK))
					.orElseGet(() -> new ResponseEntity(HttpStatus.NOT_FOUND));
	}

	private ClienteDTO converter(ClienteEntity clienteEntity) {
		return new ClienteDTO(clienteEntity);
	}
	
}
