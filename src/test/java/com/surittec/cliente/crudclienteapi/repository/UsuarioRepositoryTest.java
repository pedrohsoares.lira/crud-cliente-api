package com.surittec.cliente.crudclienteapi.repository;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;
import com.surittec.cliente.crudclienteapi.model.enuns.Perfil;

@ExtendWith( SpringExtension.class )
@ActiveProfiles("test")
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class UsuarioRepositoryTest {
	
	@Autowired
	UsuarioRespository repository;
	
	@Autowired
	TestEntityManager entityManager;
	
	@Test
	public void deveVerificarAExistenciaDeUmUsuarioAdmin() {
		boolean result = repository.existsByLogin("admin");
		Assertions.assertThat(result).isTrue();
	}
	
	@Test
	public void deveVerificarAExistenciaDeUmUsuarioComum() {
		boolean result = repository.existsByLogin("comum");
		Assertions.assertThat(result).isTrue();
	}
	
	@Test
	public void deveRetornarFalsoQuandoNaoHouverUsusarioCadastradoComLogin() {
		repository.deleteAll();
		boolean result = repository.existsByLogin("admin");
		Assertions.assertThat(result).isFalse();
	}
	
	@Test
	public void devePersistirUmUsuarioNaBaseDeDados() {
		UsuarioEntity usuario = criarUsuario(); 
		UsuarioEntity salvo = repository.save(usuario);
		
		Assertions.assertThat(salvo.getId()).isNotNull();
	}
	
	@Test
	public void deveBuscarUmUsuarioPorLogin() {
		UsuarioEntity usuario = criarUsuario();
		entityManager.persist(usuario);
		Optional<UsuarioEntity> result = repository.findByLogin("pedro.lira");
		
		Assertions.assertThat(result.isPresent()).isTrue();
	}
	
	@Test
	public void deveRetornarVazioQuandoUmUsuarioPorLoginNaoExistirNaBase() {
		Optional<UsuarioEntity> result = repository.findByLogin("naoExiste");
		
		Assertions.assertThat(result.isPresent()).isFalse();
	}
	
	public static UsuarioEntity criarUsuario() {
		return UsuarioEntity.builder()
				.nome("Pedro Lira")
				.login("pedro.lira")
				.password("123456")
				.perfil(Perfil.ADMINISTRADOR)
				.stAtivo(true)
				.build();
	}

}
