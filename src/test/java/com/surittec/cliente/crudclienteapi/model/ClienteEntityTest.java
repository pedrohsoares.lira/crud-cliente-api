package com.surittec.cliente.crudclienteapi.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.util.Assert;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class ClienteEntityTest {

	@Test
	void criarClienteVazio() {
		ClienteEntity cliente = new ClienteEntity();
		Assert.notNull(cliente, "Deveria criar um objeto de cliente.");;
	}
	
	@Test
	void criarClienteComOscamposPreenchidos() {
		ClienteEntity cliente = new ClienteEntity();
		cliente.setId(1L);
		cliente.setNome("Pedro Henrique");
		cliente.setCpf("024.065.991-08");
	}
	
}
