package com.surittec.cliente.crudclienteapi.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.surittec.cliente.crudclienteapi.exception.ErroAutenticacao;
import com.surittec.cliente.crudclienteapi.exception.RegraNegocioException;
import com.surittec.cliente.crudclienteapi.model.UsuarioEntity;
import com.surittec.cliente.crudclienteapi.model.enuns.Perfil;
import com.surittec.cliente.crudclienteapi.repository.UsuarioRespository;
import com.surittec.cliente.crudclienteapi.service.impl.UsuarioServiceImpl;

@ExtendWith( SpringExtension.class )
@SpringBootTest
@ActiveProfiles("test")
public class UsuarioServiceTest {
	
	private static final String SENHA = "123456";

	private static final String NOME_USUARIO_COMPLETO = "Pedro Henrique Soares Lira";

	private static final String LOGIN = "pedro.lira";

	@SpyBean
	UsuarioServiceImpl service;
	
	@MockBean
	UsuarioRespository repository;
	
	@Test
	public void deveSalvarUmUsuario() {
		Mockito.doNothing().when(service).validarLogin(Mockito.anyString());
		UsuarioEntity usuario = criaUsuarioParaTeste(LOGIN, SENHA, NOME_USUARIO_COMPLETO);
		Mockito.when(repository.save(Mockito.any(UsuarioEntity.class))).thenReturn(usuario);
		
		UsuarioEntity salvo = service.salvarUsuario(new UsuarioEntity());
	
		Assertions.assertThat(salvo).isNotNull();
		Assertions.assertThat(salvo.getLogin()).isEqualTo(LOGIN);
		Assertions.assertThat(salvo.getNome()).isEqualTo(NOME_USUARIO_COMPLETO);
		Assertions.assertThat(salvo.getPassword()).isEqualTo(SENHA);
		Assertions.assertThat(salvo.getStAtivo()).isTrue();
		Assertions.assertThat(salvo.getPerfil()).isEqualTo(Perfil.ADMINISTRADOR);
	}
	
	@Test
	public void naoDeveSalvarUsuarioComLoginJaCadastrado() {
		UsuarioEntity usuario = criaUsuarioParaTeste(LOGIN, SENHA, NOME_USUARIO_COMPLETO);
		Mockito.doThrow(RegraNegocioException.class).when(service).validarLogin(LOGIN);
		
		assertThrows(RegraNegocioException.class, () -> {
			service.salvarUsuario(usuario);
		});
		Mockito.verify(repository, Mockito.never()).save(usuario);
	}
	
	@Test
	public void deveAutenticarUmUsuarioComSucesso() {
		String login = LOGIN;
		String senha = SENHA;
		String nome = NOME_USUARIO_COMPLETO;
		
		UsuarioEntity usuario = criaUsuarioParaTeste(login, senha, nome);
		Mockito.when(repository.findByLogin(login)).thenReturn(Optional.of(usuario));
		
		UsuarioEntity resultado = service.autenticar(login, senha);
		
		Assertions.assertThat(resultado).isNotNull();
	}

	private UsuarioEntity criaUsuarioParaTeste(String login, String senha, String nome) {
		return UsuarioEntity.builder().login(login).nome(nome).password(senha).perfil(Perfil.ADMINISTRADOR).stAtivo(true).build();
	}
	
	@Test
	public void deveLancarErroQuandoNaoEncontrarUsuarioCadastradoComLoginInformado() {
		Mockito.when(repository.findByLogin(Mockito.anyString())).thenReturn(Optional.empty());
		
		ErroAutenticacao erro = assertThrows(ErroAutenticacao.class, () -> {
			service.autenticar(LOGIN, SENHA);
		});
		assertEquals("Usuário não encontrado para o Login informado.", erro.getMessage());
	}
	
	@Test
	public void deveLancarErroQuandoASenhaNaoBater() {
		String login = LOGIN;
		String senha = SENHA;
		String nome = NOME_USUARIO_COMPLETO;
		UsuarioEntity usuario = criaUsuarioParaTeste(login, senha, nome);
		
		Mockito.when(repository.findByLogin(Mockito.anyString())).thenReturn(Optional.of(usuario));
		
		ErroAutenticacao erro = assertThrows(ErroAutenticacao.class, () -> {
			service.autenticar(login, "senhaErrada");
		});
		
		assertEquals("Senha inválida.", erro.getMessage());
	}
	
	@Test
	public void deveValidarLogin() {
		Mockito.when(repository.existsByLogin(Mockito.anyString())).thenReturn(false);
		service.validarLogin("admin");
	}
	
	@Test
	public void deveLancarErroAoValidarLoginQuandoExistirLoginCadastrado() {
		Mockito.when(repository.existsByLogin(Mockito.anyString())).thenReturn(true);
		RegraNegocioException erro = assertThrows(RegraNegocioException.class, () -> {
			service.validarLogin("Pedro");
		});
		assertEquals("Já existe um usuario cadastrado com esse login", erro.getMessage());
	}

}
